package com.company;

import java.lang.reflect.Array;
import java.util.*;

public class Lab1 {

    private static final int LENGTH = 5_000_000;

    private static final int MAX_NUMBER = 1000;
    private static final int COUNT_THREADS = 2;

    public void calculate() throws InterruptedException {
        for (int k = 0; k < 3; k++){ //количество потоков
            for (int i = 1; i <=4; i++) { //длина массива
                int newlen = LENGTH*i;
                int newcount = COUNT_THREADS + k;
                System.out.println("Length: " + newlen);
                System.out.println("Threads: " + newcount);
                int [] mass = new int [newlen];
                for (int j = 0; j < newlen; j ++) {
                    mass[j] = (int) (Math.random()*MAX_NUMBER * i);
                }

                singleThread(mass);


                multiThread(mass,  newlen, newcount);


                System.out.println("-----------------------------------");
            }
        }


    }

    private void singleThread(int [] mass) {
        long start = System.currentTimeMillis();
        HashSet<Integer> set = new HashSet<>();
        for (var a: mass
        ) {
            set.add(a);
        }
        System.out.println("Unique values: " + set.size());
        long end = System.currentTimeMillis();
        System.out.println("Single thread: " + (end - start) + "ms");
    }

    private void multiThread(int[] mass, int len, int newcount) throws InterruptedException {
        long start = System.currentTimeMillis();
        ArrayList<Set<Integer>> setsList = new ArrayList<>();
        for (int i = 0; i < newcount; i++) {
            setsList.add(new HashSet<>());
        }
        ArrayList<Thread> threads = new ArrayList<>();
        for (int i = 0; i < newcount; i ++) {
            Thread thread = new Thread(new MyThread(Arrays.copyOfRange(mass, len*i/newcount, len*(i+1)/newcount ), setsList.get(i)));
            thread.start();
            threads.add(thread);
        }

        for (var t: threads
        ) {
            t.join();
        }

        Set<Integer> mainSet = new HashSet<>();
        for (var s : setsList
             ) {
            mainSet.addAll(s);
        }

        System.out.println("Unique values: " + mainSet.size());

        long end = System.currentTimeMillis();
        System.out.println("Multithreading: " + (end - start) + "ms");

    }

    private class MyThread implements Runnable  {

        int [] mass;
        Set<Integer> mainSet;

        public MyThread(int [] mass, Set<Integer> mainSet) {
            this.mass = mass;
            this.mainSet = mainSet;
        }

        @Override
        public void run() {
            for (var a: mass
            ) {
                mainSet.add(a);
            }
            System.out.println("Thread stopped");
        }
    }
}
